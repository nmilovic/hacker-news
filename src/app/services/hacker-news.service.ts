import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { Story } from '../components/story/model/story.model';
import { Comment } from '../components/comment/comment/model/comment.model';

@Injectable({
  providedIn: 'root'
})
export class HackerNewsService {

  constructor(private http: HttpClient) { }

  getAllStories(): Observable<number[]>{
    return this.http.get<Array<number>>('https://hacker-news.firebaseio.com/v0/topstories.json').pipe(map(
      data => {
        return data;
      }
    ));
  }

  getStory(ids?: number[]): Observable<Story[]>{
    let response = [];
    for(let i = 0; i < ids.length; i++){
      response[i] = this.http.get('https://hacker-news.firebaseio.com/v0/item/' + ids[i] + '.json');
    };
    
    return  forkJoin(response).pipe(map(data => {
      return data.map(elem => new Story(elem));
    }));
  }

  getOneStory(id?: number): Observable<Story>{
    return this.http.get('https://hacker-news.firebaseio.com/v0/item/' + id + '.json').pipe(map(data => {
      return new Story(data);
    }));
  }

  getComments(ids?: number[]): Observable<Comment[]>{
    let response = [];
    for(let i = 0; i < ids.length; i++){
      response[i] = this.http.get('https://hacker-news.firebaseio.com/v0/item/' + ids[i] + '.json');
    };
    
    return  forkJoin(response).pipe(map(data => {
      return data.map(elem => new Comment(elem));
    }));
  }
}
