export class Comment{
    by: string;
    id: number;
    kids: number[];
    parent: number;
    text: string;
    time: number;
    timeAgo: string | number;
    type: string;
    toggle: boolean;

    constructor(obj?: any){
        this.by = obj && obj.by || '';
        this.id = obj && obj.id || null;
        this.kids = obj && obj.kids || [];
        this.parent = obj && obj.parent || null;
        this.text = obj && obj.text || '';
        this.time = obj && obj.time || null;
        this.type = obj && obj.type || '';
        this.timeAgo = this.getTimeAgo();
        this.toggle = true;
    }

    getTimeAgo(){
        let currentTime = Date.now();
        let hours = (currentTime - this.time*1000)/(1000*60*60);
        if(hours >= 1){
            return Math.floor(hours) + ' hours ago';
        }else if(hours < 1){
            let minutes = hours*60;
            if(minutes < 1){
                let secounds = minutes*60;
                if(secounds < 1){
                    return ' Right now';
                }else if(secounds > 1){
                    return Math.floor(secounds) + ' secounds ago';
                }
            }else{
                return Math.floor(minutes) + ' minutes ago';
            }
        }
    }
}