import { Component, OnInit, Input } from '@angular/core';
import { Comment } from './model/comment.model';
import { HackerNewsService } from 'src/app/services/hacker-news.service';

@Component({
  selector: 'hn-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() comment: Comment;
  childComments: Comment[] = [];
  showSpinner: boolean = true;
  showComment: boolean = false;

  constructor(private hackerNewsService: HackerNewsService) { }

  ngOnInit() {
    if(this.comment.kids){
      this.hackerNewsService.getComments(this.comment.kids).subscribe(
        data => {
          this.childComments = data;
        }
      )
    }
  }

  toggle(comment: Comment){
    if(comment.toggle === true){
      comment.toggle = false;
    }else{
      comment.toggle = true;
    }
  }

}
