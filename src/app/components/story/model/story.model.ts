export class Story{
    by: string;
    descendants: number;
    id: number;
    kids: number[];
    score: number;
    time: number;
    timeAgo: number | string;
    title: string;
    type: string;
    url: string;

    constructor(obj?){
        this.by = obj && obj.by || '';
        this.descendants = obj && obj.descendants || null;
        this.id = obj && obj.id || null;
        this.kids = obj && obj.kids || [];
        this.score = obj && obj.score || null;
        this.time = obj && obj.time || null;
        this.title = obj && obj.title || '';
        this.type = obj && obj.type || '';
        this.url = obj && obj.url || '';
        this.timeAgo = this.getTimeAgo();
    }

    getTimeAgo(){
        let currentTime = Date.now();
        let hours = (currentTime - this.time*1000)/(1000*60*60);
        if(hours >= 1){
            return Math.floor(hours) + ' hours ago';
        }else if(hours < 1){
            let minutes = hours*60;
            if(minutes < 1){
                let secounds = minutes*60;
                if(secounds < 1){
                    return ' Right now';
                }else if(secounds > 1){
                    return Math.floor(secounds) + ' secounds ago';
                }
            }else{
                return Math.floor(minutes) + ' minutes ago';
            }
        }
    }
}