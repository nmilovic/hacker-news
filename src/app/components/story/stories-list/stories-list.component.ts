import { Component, OnInit } from '@angular/core';
import { HackerNewsService } from 'src/app/services/hacker-news.service';
import { Story } from '../model/story.model';

@Component({
  selector: 'hn-stories-list',
  templateUrl: './stories-list.component.html',
  styleUrls: ['./stories-list.component.css']
})
export class StoriesListComponent implements OnInit {
  topStoriesIDs: number[] = [];
  topStories: Story[] = [];
  page: number = 1;
  pageSize: number = 20;
  IDs: number;

  showSpinner: boolean = true;
  showTable: boolean = false;

  constructor(private hackerNewsService: HackerNewsService) { }

  ngOnInit() {
    this.hackerNewsService.getAllStories().subscribe(data => {
      this.topStoriesIDs = data;
      this.mapStories();
    });
  }

  changePage(newPage){
    this.showSpinner = true;
    this.showTable = false;
    this.page = newPage;
    this.mapStories(newPage);
  }

  mapStories(page: number = 1){
    let ids: number[] = [];

    for(let i = (page-1)*this.pageSize; i < page*this.pageSize; i++){
      ids.push(this.topStoriesIDs[i]);
    }
    this.hackerNewsService.getStory(ids).subscribe(data => {
      this.topStories = data;
      this.showSpinner = false;
      this.showTable = true;
    });
  }
}
