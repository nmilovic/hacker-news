import { Component, OnInit } from '@angular/core';
import { Story } from '../model/story.model';
import { HackerNewsService } from 'src/app/services/hacker-news.service';
import { ActivatedRoute } from '@angular/router';
import { Comment } from '../../comment/comment/model/comment.model';

@Component({
  selector: 'hn-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.css']
})
export class StoryComponent implements OnInit {
  story: Story;
  comments: Comment[] = [];
  showSpinner: boolean = true;
  showComments: boolean = false;

  constructor(private hackerNewsService: HackerNewsService, private route: ActivatedRoute) { }

  ngOnInit() {
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.hackerNewsService.getOneStory(id).subscribe(
      data => {
        this.story = data;
        if(this.story.kids){
          this.hackerNewsService.getComments(this.story.kids).subscribe(
            data => {
              this.comments = data;
              this.showSpinner = false;
              this.showComments = true;
            }
          );
        }
      }
    )
  }

  toggle(comment: Comment){
    if(comment.toggle === true){
      comment.toggle = false;
    }else{
      comment.toggle = true;
    }
  }

}
