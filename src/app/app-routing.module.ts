import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoriesListComponent } from './components/story/stories-list/stories-list.component';
import { StoryComponent } from './components/story/story/story.component';

const routes: Routes = [
  {path: 'stories', component: StoriesListComponent},
  {path: 'stories/:id', component: StoryComponent},
  {path: '', pathMatch:'full', redirectTo: 'stories'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
